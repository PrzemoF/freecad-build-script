#!/bin/bash

ARCH=$(arch)

MAIN_DIR=FreeCAD
BUILD_DIR=build

#FEDORA_VERSION=29
FEDORA_VERSION=30py2
#FEDORA_VERSION=30py3
#FEDORA_VERSION=31

PACKAGES="git gcc cmake gcc-c++ boost-devel zlib-devel swig eigen3 qt-devel \
shiboken shiboken-devel pyside-tools python-pyside python-pyside-devel xerces-c \
xerces-c-devel OCE-devel smesh graphviz python-matplotlib tbb-devel \
freeimage-devel Coin3 Coin3-devel med-devel vtk-devel"

FEDORA_29_PACKAGES="boost-python2 boost-python3 boost-python2-devel boost-python3-devel python-pivy"
FEDORA_30py2_PACKAGES="shiboken shiboken-python2-devel python-pyside-devel pyside-tools python-pyside python-pyside-devel boost-python2 boost-python2-devel"
#FEDORA_30py3_PACKAGES="python3-shiboken2 python3-shiboken2-devel python3-pivy python3-pyside2 boost-python3 boost-python3-devel"
#FEDORA_31_PACKAGES=""

if [ "$FEDORA_VERSION" = "29" ]; then
    PACKAGES="$PACKAGES $FEDORA_29_PACKAGES"
fi

if [ "$FEDORA_VERSION" = "30py2" ]; then
    PACKAGES="$PACKAGES $FEDORA_30py2_PACKAGES"
fi

#if [ "$FEDORA_VERSION" = "30py3" ]; then
#    PACKAGES="$PACKAGES $FEDORA_30py3_PACKAGES"
#fi

#if [ "$FEDORA_VERSION" = "31" ]; then
#    PACKAGES="$PACKAGES $FEDORA_31_PACKAGES"
#fi

echo "Installing packages required to build FreeCAD"
sudo dnf -y install $PACKAGES
cd ~
mkdir $MAIN_DIR || { echo "~/$MAIN_DIR already exist. Quitting.."; exit; }
cd $MAIN_DIR
git clone https://github.com/FreeCAD/FreeCAD.git
mkdir $BUILD_DIR || { echo "~/$BUILD_DIR already exist. Quitting.."; exit; }
cd $BUILD_DIR
cmake ../FreeCAD 
make -j$(nproc)
